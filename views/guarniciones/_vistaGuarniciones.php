<?php
use yii\helpers\ArrayHelper;
 use yii\helpers\Html;
 use yii\helpers\Url;
 
 use kartik\icons\Icon;

Icon::map($this, Icon::FA);
$this->title = 'Casa Macorra - Guarniciones';
$this->params['breadcrumbs'][] = $this->title;

$idProd = ArrayHelper::getColumn($model->productosEnGuarniciones, 'id');
$nomProd = ArrayHelper::getColumn($model->productos,'nombre');

yii\bootstrap4\Modal::begin([
    'title'=>'',
    'titleOptions'=>['class'=>"title-modal-extendido"],
    'headerOptions'=>['class'=>"header-modal-extendido"],
    'closeButton' =>['class'=>"btn btn-link boton-cerrar-modal"],
    'bodyOptions' =>['class'=>"modal-body-extendido"],
    'centerVertical'=>true,
   'id'     =>"modal$model->id",
   'size'   =>'modal-md',
   'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
   ]);
echo "<div id='modalContent$model->id'> </div>";
yii\bootstrap4\Modal::end();
?>


<div class="card shadow">
    <div class="card-header card-header-extendido" >
    <div class="row pt-2">
            <div class="col-4">
                <p class="h5 mx-3 align-middle text-uppercase"> <?= $model->nombre ?></p>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-3 text-left">
                <h5 class="text-muted mx-3">Ingredientes:</h5>
                    <p class="mx-3 m-0">
                        <?= implode('<p class="mx-3 m-0">',ArrayHelper::getColumn($model->productos,'nombre'))?>
                    </p>
            </div>
            <div class="col-3">
                <h5 class="text-muted text-right">Gramos:</h5>
                <p class="m-0 text-right">
                    <?php
                    if(ArrayHelper::getColumn($model->productosEnGuarniciones,'gramos_producto')!=null){
                        echo implode(' gr</p><p class="m-0 text-right">',ArrayHelper::getColumn($model->productosEnGuarniciones,'gramos_producto')).' gr';
                    }
                    ?>
                </p>
            </div>
            <div class="col-3">
                <h5 class="text-muted text-right">Pvp:</h5>
                <p class="m-0 text-right">
                    <?php 
                        $precio = ArrayHelper::getColumn($model->productos,'precio_compra');
                        $gproducto = ArrayHelper::getColumn($model->productosEnGuarniciones,'gramos_producto');
                        $total = 0;
                        $coste[]=0;
                        if(count($gproducto)>0){
                                for($i=0;$i<count($gproducto);$i++){
                                    $calculo = ($precio[$i] * $gproducto[$i])/60;
                                    $coste[$i] = number_format($calculo,2);
                                    $total += $coste[$i];
                            }
                        }

                        if(sizeof($coste)>0){
                            echo implode(' €</p><p class="m-0 text-right">',$coste).' €';
                       }
                    ?>
                    
                    
                    
                </p>
            </div>
            <div class="col-3 text-right">
                <h5 class="text-muted mx-3">Opciones:</h5>
                    <?php 
                        for ($i = 0; $i <= count($idProd)-1; $i++) {
                            echo'<p class="mx-3 m-0">'
                                .Html::button(Icon::show('pen', ['class' => 'text-muted fa-solid', 'framework' => Icon::FAS]),['value'=>Url::to(['productos-en-guarniciones/update2','id'=>$idProd[$i]]),
                                'class' => 'buttonmodal btn btn-link p-0',
                                'id'=>'#modalButton'.$model->id, 
                                'data-id'=>$model->id,
                                'data-tittle'=>'EDITAR CANTIDAD']).' '.
                                Html::a(Icon::show('trash', ['class' => 'text-muted fa-solid', 'framework' => Icon::FAS]), ['productos-en-guarniciones/delete', 'id' => $idProd[$i]], [
                                    'class' => 'btn btn-link p-0',
                                    'data' => [
                                        'confirm' => '¿Estás seguro que quieres eliminar '.$nomProd[$i].'?',
                                        'method' => 'post',
                                    ],
                                ]);
                        }

                    ?>
            </div>
        </div>
        <div class="col-12">
            <?= 
                Html::button(Icon::show('plus', ['class' => 'text-muted fa-solid', 'framework' => Icon::FAS])."Ingrediente",
                    ['value'=>Url::to(['productos-en-guarniciones/createid','guarnicion'=>$model->id]),
                    'class' => 'buttonmodal btn btn-link',
                    'id'=>'#modalButton'.$model->id, 
                    'data-id'=>$model->id,
                    'data-tittle'=>'Añadir ingrediente'])  
            ?>
        </div>
        <hr>
        <div class="row">
            <div class="col-6">
                <p>Coste total:</p>
            </div>
            <div class="col-6 text-right">
                <?= number_format($total, 2).' €'?>
            </div>
        </div>
        <div class="pt-4">
            <p class="text-right m-0">
            <?= 
                Html::button("Editar",['value'=>Url::to(['guarniciones/update','id'=>$model->id]),
                'class' => 'buttonmodal shadow lift btn btn-primary',
                'id'=>'#modalButton'.$model->id, 
                'data-id'=>$model->id,
                'data-tittle'=>'EDITAR GUARNICIÓN']) 
            ?>
            <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'head'=>'Confirmación',
                    'confirm' => '¿Estás seguro que quieres eliminar '.$this->title.'?',
                    'method' => 'post',
                ],
            ]) ?>
            </p>
        </div>
    </div>
</div>