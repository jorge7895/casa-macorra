<?php


/* @var $this yii\web\View */
/* @var $model app\models\Guarniciones */

?>
<div class="guarniciones-create">
    <div class="card-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
</div>
