<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Productos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="productos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true])->label('Nombre',['class'=>'texto-formularios']) ?>
    
    <?= $form->field($model, 'stock')->textInput(['maxlength' => true])->label('cantidad en stock',['class'=>'texto-formularios']) ?>
    
    <?= $form->field($model, 'precio_compra')->textInput(['maxlength' => true])->label('precio de compra',['class'=>'texto-formularios']) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'shadow lift btn btn-primary']) ?>
        <?= Html::a("Volver", ['productos/index'], ['class' => 'shadow lift btn btn-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
