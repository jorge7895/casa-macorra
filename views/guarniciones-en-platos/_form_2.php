<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\GuarnicionesEnPlatos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="guarniciones-en-platos-form">

    <?php $form = ActiveForm::begin(); ?>

    <div style="display: none;">
        <?= $form->field($model, 'id_plato')->textInput(['value'=> $plato]) ?>
    </div>

    <?= $form->field($model, 'id_guarnicion')->textInput()->dropDownList($model->getdropdownGuarniciones($plato))->label('Nombre',['class'=>'texto-formularios']) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'shadow lift btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
