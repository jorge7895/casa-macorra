<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

?>
<div class="site-about">
<div class="site-about">
    <div class="container pt-5">
        <div class="row">
            <div class="col-4">
                <div class="card shadow rounded">
                    <div class="card-header">
                         <?= Html::img('@web/images/avatar_hat.jpg',['alt'=>'Avatar','class'=>'card-img']) ?>
                         <div class="card-img-overlay text-center">
                             <h2 class="card-title">Jorge Pardo</h2>
                         </div>
                    </div>
                    <div class="card-body">

                    </div>
                </div>
            </div>
            <div class="col-8">
                <div class="card shadow rounded">
                    <div class="card-header">

                    </div>
                    <div class="card-body">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
