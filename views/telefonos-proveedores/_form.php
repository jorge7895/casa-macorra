<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TelefonosProveedores */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="telefonos-proveedores-form">

    <?php $form = ActiveForm::begin(); ?>
    <div style="display: none;">
        <?= $form->field($model, 'id_proveedor')->textInput() ?>
    </div>
    

    <?= $form->field($model, 'telefono')->textInput()->label('Teléfono',['class'=>'texto-formularios']) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'shadow lift btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro de eliminar el teléfono?',
                'method' => 'post',
            ],
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
