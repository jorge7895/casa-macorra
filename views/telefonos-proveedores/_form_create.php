<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TelefonosProveedores */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="telefonos-proveedores-form">

    <?php $form = ActiveForm::begin(); ?>
    <div style="display: none;">
        <?= $form->field($model, 'id_proveedor')->textInput(['value'=>$proveedor]) ?>
    </div>
    
    
    <?= $form->field($model, 'telefono')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'shadow lift btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
