<?php
 use yii\helpers\ArrayHelper;
 use yii\helpers\Html;
 use yii\helpers\Url;
 
 use kartik\icons\Icon;

Icon::map($this, Icon::FA);

yii\bootstrap4\Modal::begin([
    'title'=>'',
    'titleOptions'=>['class'=>"title-modal-extendido"],
    'headerOptions'=>['class'=>"header-modal-extendido"],
    'closeButton' =>['class'=>"btn btn-link boton-cerrar-modal"],
    'bodyOptions' =>['class'=>"modal-body-extendido"],
    'centerVertical'=>true,
   'id'     =>"modal$model->id",
   'size'   =>'modal-md',
   'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
   ]);
echo "<div id='modalContent$model->id'> </div>";
yii\bootstrap4\Modal::end();

$idProd = ArrayHelper::getColumn($model->productosEnPlatos, 'id');
$nomProd = ArrayHelper::getColumn($model->productos,'nombre');

$idGuarnicion = ArrayHelper::getColumn($model->guarnicionesEnPlatos, 'id');
$precioGuarnicion = ArrayHelper::getColumn($model->guarniciones, 'coste');
$nomGuarnicion = ArrayHelper::getColumn($model->guarniciones, 'nombre');

$totalIngedientes = 0;
$totalGuarniciones = 0;
?>


<div class="card shadow">
    <div class="card-header card-header-extendido">
        <div class="row pt-2">
            <div class="col-4">
                <p class="h5 mx-3 align-middle text-uppercase">Nombre:</p>
                <p class="align-middle mx-3"> <?= $model->nombre ?></p>
            </div>
            <div class="col-4 text-center">
                <p class=" h5 text-uppercase">Categoria:</p>
                <p class=""> <?= $model->categoria0->nombre ?></p>
            </div>        
            <div class="col-4 text-right">
                <p class="h5 text-uppercase">Precio:</p>
                <p class=""> <?= $model->precio_publico." €" ?></p>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-3 text-left">
                <h5 class="text-muted mx-3">Ingredientes:</h5>
                    <?php 
                        for ($i = 0; $i <= count($idProd)-1; $i++) {
                            echo '<p class="mx-3 m-0 arreglo">'.$nomProd[$i];
                        }
                    ?>
            </div>
            <div class="col-3">
                <h5 class="text-muted text-right">Gramos:</h5>
                <p class="m-0 text-right">
                    <?php 
                        if(ArrayHelper::getColumn($model->productosEnPlatos,'gramos_producto')!=null){
                            echo implode(' gr</p><p class="m-0 text-right arreglo">',ArrayHelper::getColumn($model->productosEnPlatos,'gramos_producto')).' gr';
                        }
                    ?>
                </p>
            </div>
            <div class="col-3">
                <h5 class="text-muted text-right">Pvp:</h5>
                <p class="m-0 text-right">
                    <?php 
                        $precio = ArrayHelper::getColumn($model->productos,'precio_compra');
                        $gproducto = ArrayHelper::getColumn($model->productosEnPlatos,'gramos_producto');
                        $calculo = 0;
                        
                        $coste[]=0;
                        $coste2[]=0;
                        if(count($gproducto)>0){
                                for($i=0;$i<count($gproducto);$i++){
                                    $calculo = ($precio[$i] * $gproducto[$i])/60;
                                    $coste[$i] = number_format($calculo,2);
                                    $coste2[$i] = number_format($calculo,2).' €';
                                    $totalIngedientes += floatval($coste[$i]);
                            }
                        }
                        
                    ?>
                    <?php
                        if( sizeof($coste2) >= 1) {
                            echo implode('</p><p class="m-0 text-right arreglo">',$coste2);
                        };
                     ?>
                </p>
            </div>
            <div class="col-3 text-right">
                <h5 class="text-muted mx-3">Opciones:</h5>
                    <?php 
                    if(sizeof($idProd)>0){
                        for ($i = 0; $i <= count($idProd)-1; $i++) {
                            echo '<p class="mx-3 m-0">'
                                .Html::button(Icon::show('pen', ['class' => 'text-muted fa-solid', 'framework' => Icon::FAS]),['value'=>Url::to(['productos-en-platos/update2','id'=>$idProd[$i]]),
                                'class' => 'buttonmodal btn btn-link p-0',
                                'id'=>'#modalButton'.$model->id, 
                                'data-id'=>$model->id,
                                'data-tittle'=>'Editar cantidad']).' '.
                                Html::a(Icon::show('trash', ['class' => 'text-muted fa-solid', 'framework' => Icon::FAS]), ['productos-en-platos/delete', 'id' => $idProd[$i]], [
                                    'class' => 'btn btn-link p-0',
                                    'data' => [
                                        'head'=>'Confirmación',
                                        'confirm' => '¿Estás seguro que quieres eliminar '.$nomProd[$i].'?',
                                        'method' => 'post',
                                    ],
                                ]);
                        }
                    }
                    ?>
            </div>
        </div>
        <div class="col-12">
            <?= 
                Html::button(Icon::show('plus', ['class' => 'text-muted fa-solid', 'framework' => Icon::FAS])."Ingrediente",
                ['value'=>Url::to(['productos-en-platos/createid','plato'=>$model->id]),
                'class' => 'buttonmodal btn btn-link',
                'id'=>'#modalButton'.$model->id, 
                'data-id'=>$model->id,
                'data-tittle'=>'Añadir ingrediente'])
            ?>
        </div>
        <hr>
        <div class="row">
            <div class="col-3 text-left">
                    <h5 class="text-muted mx-3">Guarniciones:</h5>
                        <?php 
                        if(sizeof($idGuarnicion)>0){
                            for ($i = 0; $i <= count($idGuarnicion)-1; $i++) {
                                echo '<p class="mx-3 m-0">'.$nomGuarnicion[$i];
                            }
                        }
                            
                        ?>
            </div>
            <div class="col-6">
                <h5 class="text-muted text-right">Pvp:</h5>
                <p class="m-0 text-right">
                    <?php 
                        if(sizeof($precioGuarnicion)>0){
                            echo implode(' €</p><p class="m-0 text-right arreglo">',$precioGuarnicion).' €';
                        }else{
                        
                        }
                        foreach ($precioGuarnicion as $key => $value) {
                            $totalGuarniciones += $value;
                        }
                    ?>
                </p>
            </div>
            <div class="col-3 text-right">
                <h5 class="text-muted mx-3">Opciones:</h5>
                    <?php 
                    if(sizeof($idGuarnicion)>0){
                        for ($i = 0; $i <= count($idGuarnicion)-1; $i++) {
                            echo '<p class="mx-3 m-0">'
                                .
                                Html::a(Icon::show('trash', ['class' => 'text-muted fa-solid', 'framework' => Icon::FAS]), ['guarniciones-en-platos/delete', 'id' => $idGuarnicion[$i]], [
                                    'class' => 'btn btn-link p-0',
                                    'data' => [
                                        'head'=>'Confirmación',
                                        'confirm' => '¿Estás seguro que quieres eliminar '.$nomGuarnicion[$i].'?',
                                        'method' => 'post',
                                    ],
                                ]);
                        }
                    }
                    ?>
            </div>
        </div>
        <div class="col-12">
            <?=
                Html::button(Icon::show('plus', ['class' => 'text-muted fa-solid', 'framework' => Icon::FAS])."Guarnición",
                ['value'=>Url::to(['guarniciones-en-platos/create2','id'=>$model->id]),
                'class' => 'buttonmodal btn btn-link',
                'id'=>'#modalButton'.$model->id, 
                'data-id'=>$model->id,
                'data-tittle'=>'Añadir guarnición']) 
            ?>
        </div>
        <hr>
        <div class="row">
            <div class="col-6">
                <p class="mx-3">Coste total:</p>
            </div>
            <div class="col-6 text-right">
                <?php 
                        echo $totalIngedientes+$totalGuarniciones.' €';
                ?>
            </div>
        </div>
        <div class="">
            <p class="text-right m-0">
            <?= 
                Html::button("Editar",['value'=>Url::to(['platos/update','id'=>$model->id]),
                'class' => 'buttonmodal shadow lift btn btn-primary',
                'id'=>'#modalButton'.$model->id, 
                'data-id'=>$model->id,
                'data-tittle'=>'Editar plato']) 
            ?>
            <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'head'=>'Confirmación',
                    'confirm' => '¿Estás seguro que quieres eliminar '.$this->title.'?',
                    'method' => 'post',
                ],
            ]) ?>
            </p>
        </div>
    </div>
</div>
