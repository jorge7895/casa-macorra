<?php

/* @var $this yii\web\View */
/* @var $model app\models\Platos */

?>
<div class="platos-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
