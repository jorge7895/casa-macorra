<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProductosEnGuarniciones */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="productos-en-guarniciones-form">

    <?php $form = ActiveForm::begin(); ?>

    <div style="display: none;">
        <?= $form->field($model, 'id_guarnicion')->dropDownList($model->getdropdownGuarniciones(),['hidden'=>'hidden']) ?>
        
    </div>
    

    <?= $form->field($model, 'id_producto')->dropDownList($model->getdropdownTodosProductos())->label('Nombre',['class'=>'texto-formularios']) ?>

    <?= $form->field($model, 'gramos_producto')->textInput(['maxlength' => true])->label('Cantidad en gramos',['class'=>'texto-formularios']) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'shadow lift btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
