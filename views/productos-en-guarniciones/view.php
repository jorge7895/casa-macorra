<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ProductosEnGuarniciones */

$this->title = $model->getNombreProducto($model->id_producto);
$this->params['breadcrumbs'][] = ['label' => 'Productos En Guarniciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="productos-en-guarniciones-view">
<div class="card shadow-sm m-3">
        <div class="card-header">
            <div class="row">
                <div class="col-6">
                    <h1>Añadido ingrediente:</h1>
                </div>
                <div class="col-6">
                    <h1 ><?= Html::encode($this->title) ?></h1>
                </div>
            </div>
        </div>
        <div class="card-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    [ 
                        'label' => 'Guarnición',
                        'value' => $model->getNombreGuarnicion($model->id_guarnicion),
                    ],
                    [ 
                        'label' => 'Ingrediente',
                        'value' => $model->getNombreProducto($model->id_producto),
                    ],
                    'gramos_producto',
                ],
            ]) ?>
           <p>
                <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => '¿Estás seguro de eliminar el registro?',
                        'method' => 'post',
                    ],
                ]) ?>
                <?= Html::a("Volver", ['guarniciones/guarnicion'], ['class' => 'shadow lift btn btn-secondary']) ?>
            </p>
        </div>
    </div>
</div>
