<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProductosDeProveedores */

$this->title = 'Añadir producto';
$this->params['breadcrumbs'][] = ['label' => 'Productos De Proveedores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productos-de-proveedores-create">
<div class="card border-macorra">
        <div class="card-header text-center text-white bg-macorra">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
        <div class="card-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>
