<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Comandas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="comandas-form">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'id_plato')->dropDownList($model->getdropdownPlato())->label('plato vendido',['class'=>'texto-formularios']) ?>

        <?= $form->field($model, 'cantidad')->textInput()->label('Cantidad vendida',['class'=>'texto-formularios']) ?>

        <div class="form-group">
            <?= Html::submitButton('Guardar', ['class' => 'shadow lift btn btn-primary']) ?>
            <?= Html::a("Volver", ['comandas/index'], ['class' => 'shadow lift btn btn-secondary']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
