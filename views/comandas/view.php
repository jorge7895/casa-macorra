<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Comandas */

$this->title = $model->fecha;
$this->params['breadcrumbs'][] = ['label' => 'Comandas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="comandas-view">
<div class="card shadow-sm m-3">
        <div class="card-header">
            <div class="row">
                <div class="col-6">
                    <h1 >Comanda del dia: <?= Html::encode(Yii::$app->formatter->asDate($this->title, 'dd-MM-yyyy')) ?></h1>
                </div>
            </div>
        </div>
        <div class="card-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'fecha',
                    'precio_total',
                    'id_plato',
                    'cantidad',
                ],
            ]) ?>
            <p>
                <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => '¿Estás seguro de eliminar el registro?',
                        'method' => 'post',
                    ],
                ]) ?>
                <?= Html::a("Volver", ['comandas/index'], ['class' => 'shadow lift btn btn-secondary']) ?>
            </p>
        </div>
    </div>


</div>
