<?php


/* @var $this yii\web\View */
/* @var $model app\models\Comandas */

?>


<div class="comandas-create">
<?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
