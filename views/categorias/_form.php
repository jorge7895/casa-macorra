<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Categorias */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="categorias-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
    <?= Html::submitButton('Guardar', ['class' => 'shadow lift btn btn-primary']) ?>
        <?= Html::a("Volver", ['categorias/index'], ['class' => 'shadow lift btn btn-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
