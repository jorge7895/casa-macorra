<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Categorias */

$this->title = Yii::t('app', '{name}', [
    'name' => $model->nombre,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Categorias'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="categorias-update">
<div class="card shadow-sm m-3">
        <div class="card-header">
            <div class="row">
                <div class="col-6">
                    <h1>Actualizar:</h1>
                </div>
                <div class="col-6">
                    <h1 class="text-right"><?= Html::encode($this->title) ?></h1>
                </div>
            </div>
        </div>
        <div class="card-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>
