<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Pedidos */

$this->title = $model->fecha;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pedidos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="pedidos-view">
<div class="card shadow-sm m-3">
        <div class="card-header">
            <div class="row">
                <div class="col-6">
                    <h1 >Pedido fecha:</h1>
                </div>
                <div class="col-6">
                    <h1 class="text-right"><?= Html::encode($this->title) ?></h1>
                </div>
            </div>
        </div>
        <div class="card-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'fecha',
                    'id_producto',
                    'id_proveedor',
                    'descuento',
                    'cantidad',
                ],
            ]) ?>
            <p>
                <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
                <?= Html::a("Volver", ['pedidos/index'], ['class' => 'shadow lift btn btn-secondary']) ?>
            </p>
        </div>
    </div>
</div>
