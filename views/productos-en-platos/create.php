<?php



/* @var $this yii\web\View */
/* @var $model app\models\ProductosEnPlatos */

?>
<div class="productos-en-platos-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
