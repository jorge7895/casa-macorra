<?php

?>
<div class="proveedores-update">
<div class="card shadow-sm m-3">
        <div class="card-header card-header-extendido">
            <div class="row">
                <div class="col-6">
                    <h1>Actualizar:</h1>
                </div>
                <div class="col-6">
                    <h1 class="text-right"><?= $model->nombre ?></h1>
                </div>
            </div>
        </div>
        <div class="card-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>
