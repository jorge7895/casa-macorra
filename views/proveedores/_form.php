<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

use kartik\icons\Icon;

Icon::map($this, Icon::FA);

yii\bootstrap4\Modal::begin([
    'title'=>'',
    'titleOptions'=>['class'=>"title-modal-extendido"],
    'headerOptions'=>['class'=>"header-modal-extendido"],
    'closeButton' =>['class'=>"btn btn-link boton-cerrar-modal"],
    'bodyOptions' =>['class'=>"modal-body-extendido"],
    'centerVertical'=>true,
   'id'     =>"modal$model->id",
   'size'   =>'modal-md',
   'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
   ]);
echo "<div id='modalContent$model->id'> </div>";
yii\bootstrap4\Modal::end();
?>

<div class="proveedores-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nif')->textInput(['maxlength' => true]) ?>

    

    <?php 
    
        try {
            $data = $model->getTelefonosProveedores($model->id);

            
            if(sizeof($data)>1){

                for ($i=1; $i <= sizeof($data); $i++) { 
                    echo '<label >Teléfono: '.($i).'</label><br>'.'<input disabled class="form-control" value='.$data[$i].'>'.
                    Html::button('Editar '.Icon::show('pen', ['class' => 'fa-solid', 'framework' => Icon::FAS]),['value'=>Url::to(['telefonos-proveedores/update','id'=>key($data)]),
                    'class' => 'buttonmodal shadow lift btn-sm btn-primary float-right m-1',
                    'id'=>'#modalButton'.$model->id, 
                    'data-id'=>$model->id,
                    'data-tittle'=>'Editar teléfono']).'<br>';

                }
            }else{
                echo '<label >Teléfono: </label><br>'.'<input disabled class="form-control" value='.$data[key($data)].'>'.
                    Html::button('Editar '.Icon::show('pen', ['class' => 'fa-solid', 'framework' => Icon::FAS]),['value'=>Url::to(['telefonos-proveedores/update','id'=>key($data)]),
                    'class' => 'buttonmodal shadow lift btn-sm btn-primary float-right m-1',
                    'id'=>'#modalButton'.$model->id, 
                    'data-id'=>$model->id,
                    'data-tittle'=>'Editar teléfono']).'<br>';
            }
        } catch (\Throwable $th) {
        }
        
    ?>


    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'shadow lift btn btn-primary']) ?>
        <?= Html::button('Añadir teléfono '.Icon::show('pen', ['class' => 'fa-solid', 'framework' => Icon::FAS]),['value'=>Url::to(['telefonos-proveedores/create2','id'=>$model->id]),
                'class' => 'buttonmodal shadow lift btn btn-primary',
                'id'=>'#modalButton'.$model->id, 
                'data-id'=>$model->id, 
                'data-tittle'=>'Añadir teléfono']) ?>
        <?= Html::a("Volver", ['proveedores/index'], ['class' => 'shadow lift btn btn-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
