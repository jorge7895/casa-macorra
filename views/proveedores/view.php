<?php

use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Proveedores */

?>
<div class="proveedores-view">
    <div class="row row-space m-3">
        <div class="col-12">
            <div class="card shadow-sm">
                <?php Pjax::begin(); ?>
                    <?php
                        echo \kartik\grid\GridView::widget([
                                'dataProvider' => $dataProvider,
                                'columns' => $gridColumns,
                                'beforeHeader'=>false,
                                'toolbar' =>  false,
                                'pjax' => false,
                                'bordered' => false,
                                'striped' => false,
                                'condensed' => false,
                                'responsive' => true,
                                'hover' => true,
                                'floatHeader' => true,
                                'showPageSummary' => false,
                                'summary'=>false,
                                'pageSummaryContainer' => false,
                                'persistResize' => false,
                                'panel' => [
                                    'heading'=>'<h3 class="panel-title">'.$nombreProveedor[0]['nombre'].'</h3>',
                                    'headingOptions'=>['class'=>'panel-heading table-color rounded-top'],
                                    'footer' => false,
                                    'after'=>false,
                                    'before'=> false
                                ],
                                'afterHeader'=>[
                                    [
                                        'options'=>['class'=>'skip-export']
                                    ]
                                ],
                        ]);
                    ?>
                <?php Pjax::end(); ?>
                <div class="ml-3 mt-3 mb-0">
                    <p>
                        <?= Html::a('Update', ['update', 'id' => $id], ['class' => 'btn btn-primary']) ?>
                        <?= Html::a('Delete', ['delete', 'id' => $id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => '¿Seguro que quieres eliminar el elemento?',
                                'method' => 'post',
                            ],
                        ]) ?>
                        <?= Html::a("Volver", ['proveedores/index'], ['class' => 'shadow lift btn btn-secondary']) ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
